$(function() {
	$("body").click(function(event) {
		if (!event.target.matches('.dropbtn')) {
			$("#myProfileDD").toggleClass("show", false);
		}
	});
	
	$("#aSignOut").click(function(){
		Cookies.remove('sessionId', { path: '/' });
		Cookies.remove('userType', { path: '/' });
		window.location.href = "Login.html";
	});
	
	var userType = Cookies.get('userType');
	
	if (userType == "admin" || userType == "consultant") {
		$("#aManageUser").remove();
	}
	
	/* uncomment this section for testing with your service
	var sessionId = Cookies.get('sessionId');
	if (typeof sessionId === "undefined) {	
		$.ajax({
			url: "service.svc?listFiles", // service method goes here
			data: { "sessionId": sessionId },
			dataType: "json",
			success: function(data, txtStatus, jqXHR) {
				if (typeof data.superAdmin === "undefined") {
					$("#divSuperAdmin").remove();
				} else {
					$.each(data.superAdmin, function(sKey, sValue) {
						$("#divSuperAdmin").append("<button class='ui-button ui-widget ui-corner-all btnSAdmin'>" + sValue.name + "</button>");
					});
				}
				
				if (typeof data.admin === "undefined") {
					$("#divAdmin").remove();
				} else {
					$.each(data.admin, function(aKey, aValue) {
						$("#divAdmin").append("<button class='ui-button ui-widget ui-corner-all btnAAdmin'>" + aValue.name + "</button>");
					});
				}
				
				if (typeof data.fileList === "undefined") {
					$("#divGridView").remove();
				} else {
					$.each(data.fileList, function(fKey, fValue) {
						$("#tblFileList").append("<tr><td>" + (fKey + 1) + "</td><td>" + fValue.filename + "</td><td>" + fValue.dateUpdated + "</td><td><button class='ui-button ui-widget ui-corner-all'>Delete</Button></td></tr>");
					});
				}
			}
		});	
	} else {
		window.location.href = "login.html";
	}
	*/
	
	//start region : comment the below code while testing with service
	var manageFilesData = { superAdmin : [{ id : 1, name : "Admin 1" }, { id : 2, name : "Admin 2"}, { id : 3, name : "Admin 3"}], admin : [{ id : 1, name : "Consultant 1" }, { id : 2, name : "Consultant 2"}], fileList : [{ filename: "abc.png", dateUpdated : "23-02-2017"}, { filename: "xyz.png", dateUpdated : "23-02-2017"}]};
	
	if (typeof manageFilesData.superAdmin === "undefined") {
		$("#divSuperAdmin").remove();
	} else {
		$.each(manageFilesData.superAdmin, function(sKey, sValue) {
			$("#divSuperAdmin").append("<button class='ui-button ui-widget ui-corner-all btnSAdmin'>" + sValue.name + "</button>");
		});
	}
	
	if (typeof manageFilesData.admin === "undefined") {
		$("#divAdmin").remove();
	} else {
		$.each(manageFilesData.admin, function(aKey, aValue) {
			$("#divAdmin").append("<button class='ui-button ui-widget ui-corner-all btnAAdmin'>" + aValue.name + "</button>");
		});
	}
	
	if (typeof manageFilesData.fileList === "undefined") {
		$("#divGridView").remove();
	} else {
		$.each(manageFilesData.fileList, function(fKey, fValue) {
			$("#tblFileList").append("<tr><td>" + (fKey + 1) + "</td><td>" + fValue.filename + "</td><td>" + fValue.dateUpdated + "</td><td><button class='ui-button ui-widget ui-corner-all'>Delete</Button></td></tr>");
		});
	}
	// end region
});

function viewDropdown() {
	$("#myProfileDD").toggleClass("show");
}