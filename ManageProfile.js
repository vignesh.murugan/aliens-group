var myProfile;

$(function() {
    $("body").click(function(event) {
		if (!event.target.matches('.dropbtn')) {
			$("#myProfileDD").toggleClass("show", false);
		}
	});
	
	$("#aSignOut").click(function(){
		Cookies.remove('sessionId', { path: '/' });
		Cookies.remove('userType', { path: '/' });
		window.location.href = "Login.html";
	});
	
	var userType = Cookies.get('userType');
	
	if (userType == "admin" || userType == "consultant") {
		$("#aManageUser").remove();
	}
	
	$(".controlgroup").controlgroup();
	
	$("#btnSaveDetails").click(function(){
		clearMsgLabel();
		removeValidationErr();
		if (isValidProfile()) {
			/*
			var sessionId = Cookies.get('sessionId');
			var userType = Cookies.get('userType');
			var uName = $("#txtName").val();
			var phNo = $("#txtMobileNo").val();
			var cmpyName = $("#txtCompanyName").val();
			var jsonData;
			
			if ($.trim(userType).toLowerCase() == "consultant") {
				jsonData = { 'sessionId': sessionId, 'name': uName, 'phoneNo': phNo, 'companyName': cmpyName };
			} else {
				jsonData = { 'sessionId': sessionId, 'name': uName, 'phoneNo': phNo };
			}
	
			$.ajax({
				url: "service.svc?saveProfile", // service method goes here
				data: jsonData,
				dataType: "json",
				success: function(data, txtStatus, jqXHR) {
					if(data.success === true) {
						setMsgLbl(false, data.message);
						updateMyProfile(uName, phNo, cmpyName); 
					} else {
						setMsgLbl(true, data.message);
					}
				}
			});
			*/
		}
	});
	
	$("#btnCancel").click(function(){
		clearMsgLabel();
		removeValidationErr();
		fillMyProfile();
	});
	
	// comment the below lines while testing with service
	myProfile = { success: true, name : "My User Name", email : "myemailid@asdf.com", phoneNo : "9898989887", userType : "Consultant", companyName : "My company name" };
	//myProfile = { success: true, name : "My User Name", email : "myemailid@asdf.com", phoneNo : "9898989887", userType : "Admin" };	
	fillMyProfile();
	// end of comment
	
	// uncomment the below code while testing with service
	// the expected json format for successful profile is 
	// { success: true, name : "My User Name", email : "myemailid@asdf.com", phoneNo : "9898989887", userType : "Consultant", companyName : "My company name" }
	// or
	// { success: true, name : "My User Name", email : "myemailid@asdf.com", phoneNo : "9898989887", userType : "Admin" }
	/*
	$.ajax({
		url: "service.svc?getProfile", // service method goes here
		data: jsonData,
		dataType: "json",
		success: function(data, txtStatus, jqXHR) {
			if(data.success === true) {
				myProfile = data;
				fillMyProfile();
			} else {
				setMsgLbl(true, data.message);
			}
		}
	});
	*/
	// end of uncomment
	
	$("#txtName,#txtMobileNo,#txtCompanyName").on("focus", function() {
		removeErrorMsg($(this));
	});
});

function setMsgLbl(isErr, msg) {
	$("#divMsgLbl").toggleClass("successMsgLbl", !isErr).toggleClass("errMsgLbl", isErr).html(msg);
}

function removeErrorMsg(control) {
	control.toggleClass("txtBoxErr", false).removeAttr("title").removeAttr("alt");
}

function setErrorMsg(control, msg) {
	control.toggleClass("txtBoxErr", true).attr("title", msg).attr("alt", msg);
}

function clearMsgLabel() {
	$("#divMsgLbl").html('');
}

function updateMyProfile(uName, phoneNo, cmpyName) {
	myProfile.name = uName;
	myProfile.phoneNo = phoneNo;
	myProfile.companyName = cmpyName;
}

function isValidProfile() {
	var txtName = $("#txtName");
	var uName = txtName.val();
	var txtMobileNo = $("#txtMobileNo");
	var phNo = txtMobileNo.val();
	var txtCompanyName = $("#txtCompanyName");
	var cmpyName = txtCompanyName.val();
	var isValid = true;
	
	if ($.trim(uName) == '') {
		setErrorMsg(txtName, "Please enter name");
		isValid = false;
	}
	if ($.trim(phNo) == '') {
		setErrorMsg(txtMobileNo, "Please enter mobile number");
		isValid = false;
	} else {
		if(!isValidPhoneNum(phNo)) {
			setErrorMsg(txtMobileNo, "Please enter valid mobile number");
			isValid = false;
		}
	}
	if (myProfile.companyName !== "undefined" && $.trim(cmpyName) == '') {
		setErrorMsg(txtCompanyName, "Please enter company name");
		isValid = false;
	}
	return isValid;
}

function removeValidationErr() {
	removeErrorMsg($("#txtName"));
	removeErrorMsg($("#txtMobileNo"));
	removeErrorMsg($("#txtCompanyName"));
}

function fillMyProfile() {
	$("#txtName").val(myProfile.name);
	$("#spnEmail").html(myProfile.email);
	$("#txtMobileNo").val(myProfile.phoneNo);
	$("#spnUserType").html(myProfile.userType);
	if (myProfile.userType.toLowerCase() == "admin" || myProfile.userType.toLowerCase() == "superadmin" || myProfile.companyName === "undefined") {
		$("#divCmpyName").hide();
	} else {
		$("#txtCompanyName").val(myProfile.companyName);
	}
}

function isValidPhoneNum(phNo) {
	var re = /(?:\s+|)((0|(?:(\+|)91))(?:\s|-)*(?:(?:\d(?:\s|-)*\d{9})|(?:\d{2}(?:\s|-)*\d{8})|(?:\d{3}(?:\s|-)*\d{7}))|\d{10})(?:\s+|)/;
    return re.test(phNo);
}

function viewDropdown() {
	$("#myProfileDD").toggleClass("show");
}