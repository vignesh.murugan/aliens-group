$(function() {
	$("body").click(function(event) {
		if (!event.target.matches('.dropbtn')) {
			$("#myProfileDD").toggleClass("show", false);
		}
	});
	
	$("#aSignOut").click(function(){
		Cookies.remove('sessionId', { path: '/' });
		Cookies.remove('userType', { path: '/' });
		window.location.href = "Login.html";
	});
	
	var userType = Cookies.get('userType');
	
	if (userType == "admin" || userType == "consultant") {
		$("#aManageUser").remove();
	}
	
    $(".controlgroup").controlgroup();
	
	$("#divCmpyName").hide();
	
	$("#selUserType").selectmenu({
	  change: function(event, ui) {
		if(ui.item.value == "1") {
			$("#divCmpyName").hide();
		} else {
			$("#divCmpyName").show();
		}
	  }
	});	
	
	$("#btnCreateUser").click(function(){
		clearMsgLabel();
		removeValidationErr();
		if (validateInput()) {
			/*
			var sessionId = Cookies.get('sessionId');
			var uName = $.trim($("#txtName").val());
			var email = $.trim($("#txtEmail").val());
			var phNo = $.trim($("#txtMobileNo").val());
			var newUserType = ($("#selUserType").val()==1)? "Admin": "Consultant";
			var cmpyName = $.trim($("#txtCompanyName").val());
			
			var jsonData = { 'sessionId': sessionId, 'name': uName, 'email' : email, 'phoneNo': phNo, 'userType' : newUserType, 'companyName': cmpyName };
			
	
			$.ajax({
				url: "service.svc?createUser", // service method goes here
				data: jsonData,
				dataType: "json",
				success: function(data, txtStatus, jqXHR) {
					if(data.success === true) {
						setMsgLbl(false, data.message);
					} else {
						setMsgLbl(true, data.message);
					}
				}
			});
			*/
		}
	});
	
	$("#txtEmail,#txtMobileNo").on("focus", function() {
		removeErrorMsg($(this));
	});
});

function setMsgLbl(isErr, msg) {
	$("#divMsgLbl").toggleClass("successMsgLbl", !isErr).toggleClass("errMsgLbl", isErr).html(msg);
}

function clearMsgLabel() {
	$("#divMsgLbl").html('');
}

function removeValidationErr() {
	removeErrorMsg($("#txtEmail"));
	removeErrorMsg($("#txtMobileNo"));
}

function removeErrorMsg(control) {
	control.toggleClass("txtBoxErr", false).removeAttr("title").removeAttr("alt");
}

function setErrorMsg(control, msg) {
	control.toggleClass("txtBoxErr", true).attr("title", msg).attr("alt", msg);
}

function viewDropdown() {
	$("#myProfileDD").toggleClass("show");
}

function validateInput() {
	var txtEmail = $("#txtEmail");
	var email = txtEmail.val();
	var txtMobileNo = $("#txtMobileNo");
	var phoneNo = txtMobileNo.val();
	var isValid = true;
	
	if ($.trim(email) == '') {
		setErrorMsg(txtEmail, "Please enter email id");
		isValid = false;
	} else {
		if (!isValidEmail(email)) {
			setErrorMsg(txtEmail, "Please enter valid email id");
			isValid = false;
		}
	}
	
	if ($.trim(phoneNo) != '') {
		if (!isValidPhoneNum(phoneNo)) {
			setErrorMsg(txtMobileNo, "Please enter valid phone number");
			isValid = false;
		}
	}
	return isValid;
}

function isValidPhoneNum(phNo) {
	var re = /(?:\s+|)((0|(?:(\+|)91))(?:\s|-)*(?:(?:\d(?:\s|-)*\d{9})|(?:\d{2}(?:\s|-)*\d{8})|(?:\d{3}(?:\s|-)*\d{7}))|\d{10})(?:\s+|)/;
    return re.test(phNo);
}

function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
