$(function() {
    $(".controlgroup").controlgroup();
	
	$("#btnLogin").click(function(){
		$("#divMsgLbl").html("");
		if (validateInput()) {
			var UName = $("#txtEmail").val();
			var pswd = $("#txtPswd").val();
			// start region : comment the below line for testing with service
			window.location.href = "ManageFiles.html";
			// end region
			
			// uncomment the below code for testing with service
			// the expected json format for successful login is { "success" : true, "sessionId": "sessionIdOfTheUser" }
			/*
			$.ajax({
				url: "service.svc?login", // service method goes here
				data: { name: UName, password: pswd },
				dataType: "json",
				success: function(data, txtStatus, jqXHR) {
					if(data.success === true) {
						Cookies.set('sessionId', data.sessionId, { expires: 1, path: '/' });
						Cookies.set('userType', data.userType, { expires: 1, path: '/' });
						window.location.href = "ManageFiles.html";
					} else {
						$("#divMsgLbl").toggleClass("errMsgLbl", true).html(data.message);
					}
				}
			});
			*/
		}
	});
	
	$("#txtEmail").on("focus", function() {
		$(this).toggleClass("txtBoxErr", false).removeAttr("title").removeAttr("alt");
	});
});

function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateInput() {
	var isValid = true;
	var txtEmail = $("#txtEmail");
	var uName = txtEmail.val();
	
	if ($.trim(uName) == '') {
		txtEmail.toggleClass("txtBoxErr", true).attr("title", "Please enter email id").attr("alt", "Please enter email id");
		isValid = false;
	} else {
		if (!isValidEmail(uName)) {
			txtEmail.toggleClass("txtBoxErr", true).attr("title", "Please enter valid email id").attr("alt", "Please enter valid email id");
			isValid = false;
		}
	}
	return isValid;
}